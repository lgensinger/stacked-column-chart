import test from "ava";

import { configurationLayout } from "@lgv/visualization-chart";
import { configuration } from "../src/configuration.js";
import { StackedColumnChart } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
//let scc = new StackedColumnChart();

// TEST INIT //
/*test("init", t => {

    t.true(scc.height === configurationLayout.height);
    t.true(scc.width === configurationLayout.width);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});*/

/******************** DECLARED PARAMS ********************/

let testWidth = 300;
let testHeight = 500;
let testData = {
    someKey: { "a": 445, "b": 4, "c": 10 },
    someKey2: { "a": 300, "b": 200, "c": 5 }
};

// initialize
let sccp = new StackedColumnChart(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(sccp.height === testHeight);
    t.true(sccp.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbcp.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
