import { max } from "d3-array";
import { scaleBand, scaleLinear } from "d3-scale";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { ChartLabel, ColumnLabel, LinearGrid, StackLayout as SL } from "@lgv/visualization-chart";

import { configuration, configurationLayout, configurationSeries } from "../configuration.js";

/**
 * StackedColumnChart is a series visualization.
 * @param {array} data - objects where each represents a series in the collection
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class StackedColumnChart extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, StackLayout=null, seriesLabels=configurationSeries.useLabels, paddingSeries=configurationSeries.padding, normalize=configurationSeries.normalize, labelsOnTop=configurationSeries.labelsOnTop, name=configuration.name, label=configuration.branding) {

        // initialize inheritance
        super(data, width, height, StackLayout ? StackLayout : new SL(data, { height: height, normalize: normalize, paddingSeries: paddingSeries, useLabels: seriesLabels, width: width }), label, name);

        // update self
        this.column = null;
        this.columnLabel = null;
        this.columnLabelLeader = null;
        this.columnLabelGroup = null;
        this.classColumn = `${label}-column`;
        this.classColumnLabel = `${label}-column-label`;
        this.classColumnLabelGroup = `${label}-column-label-group`;
        this.classColumnLabelLeader = `${label}-column-label-leader`;
        this.classColumnLabelPartial = `${label}-column-label-partial`;
        this.classSeries = `${label}-series`;
        this.classSeriesLabel = `${label}-series-label`;
        this.series = null;
        this.seriesLabel = null;

    }

    /**
     * Construct a scale to separate Column values in a series.
     * @returns A d3 scale function.
     */
    get columnScale() {

        let start = this.Data.useLabels ? (this.Data.labelsOnTop ? this.seriesLabelSpace : 0) : 0;
        let end = this.Data.useLabels ? (this.Data.labelsOnTop ? this.height : this.height - this.seriesLabelSpace) : this.height;

        return scaleLinear()
            .domain([0, this.Data.normalize ? 1 : this.Data.seriesMax])
            .range([start, end]);
    }

    /**
     * Determine space between series and its label.
     * @returns A float representing the padding between label and form.
     */
    get seriesLabelSpace() {
        return this.seriesLabelMax;
    }

    /**
     * Determine tallest series label value.
     * @returns A float representing the tallest series label letter.
     */
    get seriesLabelMax() {
        return this.Label ? max(this.Data.seriesLabels.map(k => this.Label.determineHeight(k)), d => d) : this.unit;
    }

    /**
     * Construct scale to separate series values.
     * @returns A d3 scale function.
     */
    get seriesScale() {
        return scaleBand()
            .domain(this.Data.seriesLabels)
            .rangeRound([0, this.width])
            .paddingInner(this.Data.padding);
    }

    /**
     * Determine column height.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for height value.
     */
    columnHeight(d) {
        return this.columnScale(d[0][1] - d[0][0]) - this.columnScale.range()[0];
    }

    /**
     * Determine column width.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for height value.
     */
    columnWidth(d) {
        return this.seriesScale.bandwidth();
    }

    /**
     * Determine column y.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for y value.
     */
    columnX(d) {
        return 0;
    }

    /**
     * Determine column y.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for y value.
     */
    columnY(d) {
        return this.columnScale.range()[1] - this.columnScale(d[0][1]);
    }

    /**
     * Declare Column mouse events.
     */
    configureColumnEvents() {
        this.column
            .on("click", (e,d) => this.configureEvent("column-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classColumn} active`);

                // send event to parent
                this.configureEvent("column-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classColumn);

                // send event to parent
                this.artboard.dispatch("column-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style column label groups in SVG dom element.
     */
    configureColumnLabelGroups() {
        this.columnLabelGroup
            .transition().duration(1000)
            .attr("class", this.classColumnLabelGroup)
            .attr("transform", d => {

                let result;
                let columnHeight = this.columnHeight(d);
                let columnHeightHalf = columnHeight / 2;
                let stackIsTooTall = this.Label.stackIsTooTall(d, columnHeight);
                let singleIsTooTall = this.Label.singleIsTooTall(d, columnHeight);
                let isLastSeries = this.isLastSeries(d);
                let yOffset = this.Label.yOffset(d).y;
                let labelHeight = this.Label.determineHeight(this.Data.extractLabel(d));
                let stackedLabelHeight = labelHeight * 2;
                let singleLabelWidth = this.Label.determineWidth(`${this.Data.extractLabel(d)}${this.unit / 2}${this.Data.extractValue(d)}`);
                let singleLabelWidthHalf = singleLabelWidth / 2;
                let singlePartialLabelWidth = this.Label.determineWidth(`${this.Data.extractLabel(d)}`);

                // must be leadered
                if (stackIsTooTall && singleIsTooTall) {

                    result = isLastSeries ? `translate(${-(this.seriesScale.step() / 2) + this.unit},${yOffset})` : `translate(${this.columnWidth(d) + this.unit},${yOffset})`;

                // can center first label partial in single line
                } else if (stackIsTooTall && !singleIsTooTall && singleLabelWidth > (this.columnWidth(d) * 1.1)) {

                    result = `translate(${this.Label.x(d) - (singlePartialLabelWidth / 2)}, ${this.columnY(d) + columnHeightHalf - (labelHeight * 0.7)})`;

                // can center full label in single line
                } else if (stackIsTooTall && !singleIsTooTall) {

                    result = `translate(${this.Label.x(d) - singleLabelWidthHalf}, ${this.columnY(d) + columnHeightHalf - (labelHeight * 0.7)})`;

                // default stack layout
                } else {

                    result = `translate(${this.Label.x(d)}, ${this.columnY(d) + columnHeightHalf - (stackedLabelHeight * 0.66)})`;

                }

                return result;

            });
    }

    /**
     * Position and minimally style column label leader lines in SVG dom element.
     */
    configureColumnLabelLeaders() {
        this.columnLabelLeader
            .transition().duration(1000)
            .attr("class", this.classColumnLabelLeader)
            .attr("x1", d => this.isLastSeries(d) ? (this.seriesScale.step() / 2) - (this.unit * 2) : -this.unit)
            .attr("x2", d => this.isLastSeries(d) ? (this.seriesScale.step() / 2) - this.unit : -(this.unit * 0.25))
            .attr("y1", d => this.isLastSeries(d) ? -(this.Label.determineHeight(d, this.columnHeight(d)) * 0.33) : -this.Label.yOffset(d).yDiff + this.columnHeight(d) / 2)
            .attr("y2", d => this.isLastSeries(d) ? -this.Label.yOffset(d).yDiff + this.columnHeight(d) / 2 : -(this.Label.determineHeight(d, this.columnHeight(d)) * 0.33))
            .attr("stroke", "lightgrey");
    }

    /**
     * Position and minimally style column label partials in SVG dom element.
     */
    configureColumnLabelPartials() {
        this.columnLabelPartial
            .transition().duration(1000)
            .attr("class", this.classColumnLabelPartial)
            .attr("dx", (d,i) => {

                let result;
                let columnHeight = this.columnHeight(d);
                let stackIsTooTall = this.Label.stackIsTooTall(d, columnHeight);
                let singleIsTooTall = this.Label.singleIsTooTall(d, columnHeight);

                // first of stack or either in single
                if (stackIsTooTall || i == 0) {
                    result = stackIsTooTall && i != 0 ? this.unit / 2 : null;
                // second of stack
                } else {
                    result = -this.Label.determineWidth(this.Label.format(0, this.Data.extractLabel(d), this.Data.extractValue(d), this.columnWidth(d)));
                }

                return result;

            })
            .attr("y", d => this.Label.stackIsTooTall(d, this.columnHeight(d)) && this.Label.singleIsTooTall(d, this.columnHeight(d)) ? null : this.Label.y(d, this.columnY(d)))
            .attr("dy", (d,i) => this.Label.stackIsTooTall(d, this.columnHeight(d)) ? null : this.Label.calculateDy(i,d, this.columnWidth(d)))
            .textTween((d,i) => {

                let height = this.columnHeight(d);
                let isLeadered = this.Label.stackIsTooTall(d,height) && this.Label.singleIsTooTall(d,height);

                let label = this.Data.extractLabel(d);
                let value = this.Data.extractValue(d);

                let bounds = isLeadered ? (this.seriesScale.step() / 2) - (this.unit * 2) : this.columnWidth(d);

                return this.tweenText(label, value, bounds, i)

            })
            .attr("visibility", (d,i) => {

                let height = this.columnHeight(d);
                let stackIsTooTall = this.Label.stackIsTooTall(d,height);
                let singleIsTooTall = this.Label.singleIsTooTall(d,height);

                let isLeadered = stackIsTooTall && singleIsTooTall;
                let isSingle = stackIsTooTall;

                let label = this.Data.extractLabel(d);
                let value = this.Data.extractValue(d);

                let bounds = isLeadered ? (this.seriesScale.step() / 2) - (this.unit * 2) : this.columnWidth(d);

                let isTooWide = this.Label.determineWidth(`${label}${value}${this.unit * 2}`) > bounds;

                return i == 0 ? "visible" : (isTooWide && isLeadered ? "hidden" : (isSingle && isTooWide ? "hidden" : "visible"));

            });
    }

    /**
     * Position and minimally style column label text element in SVG dom element.
     */
    configureColumnLabels() {
        this.columnLabel
            .attr("class", this.classColumnLabel)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("pointer-events", "none")
            .attr("text-anchor", d => this.Label.stackIsTooTall(d, this.columnHeight(d)) ? "start" : "middle")
    }

    /**
     * Position and minimally style columns in SVG dom element.
     */
    configureColumns() {
        this.column
            .transition().duration(1000)
            .attr("class", this.classColumn)
            .attr("data-id", d => this.Data.extractId(d))
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-series", d => this.Data.extractSeries(d))
            .attr("fill", "lightgrey")
            .attr("x", d => this.columnX(d))
            .attr("y", d => this.columnY(d))
            .attr("width", d => this.columnWidth(d))
            .attr("height", d => this.columnHeight(d));
    }

    /**
    * Position and minimally style Column group in svg dom.
    */
    configureSeries() {
        this.series
            .attr("class", this.classSeries)
            .attr("data-label", d => d.series)
            .attr("transform", d => `translate(${this.seriesScale(d.series)},${this.Data.useLabels && this.Data.labelsOnTop ? this.seriesLabelSpace : 0})`);
    }

    /**
    * Position and minimally style series label in svg dom.
    */
    configureSeriesLabels() {
        this.seriesLabel
            .transition().duration(1000)
            .attr("class", this.classSeriesLabel)
            .attr("data-label", d => d)
            .attr("x", d => this.Label.x(d))
            .attr("y", d => this.Data.labelsOnTop ? (-this.seriesLabelSpace + (this.seriesLabelMax * 0.7)) : this.height - (this.seriesLabelMax * 0.3))
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .text(d => this.Label.format(0, d, null, this.columnWidth(d)));
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ColumnLabel(this.artboard, this.Data, this.columnScale.range()[1] - this.columnScale.range()[0], this.seriesScale, this.columnHeight, this.columnScale, this.columnY);

        // generate group for each series
        this.series = this.generateSeries(this.content);
        this.configureSeries();

        // generate columns in each series
        this.column = this.generateColumns(this.series);
        this.configureColumns();
        this.configureColumnEvents();

        // generate series labels
        this.seriesLabel = this.generateSeriesLabels(this.series);
        this.configureSeriesLabels();

        // generate column label groups in each series
        this.columnLabelGroup = this.generateColumnLabelGroups(this.series);
        this.configureColumnLabelGroups();

        // generate label leader lines
        this.columnLabelLeader = this.generateColumnLabelLeaders(this.columnLabelGroup);
        this.configureColumnLabelLeaders();

        // generate column labels in each group
        this.columnLabel = this.generateColumnLabels(this.columnLabelGroup);
        this.configureColumnLabels();

        // generate label partials so they stack
        this.columnLabelPartial = this.generateColumnLabelPartials(this.columnLabel);
        this.configureColumnLabelPartials();

    }

    /**
     * Construct columns in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateColumns(domNode) {
        return domNode
            .selectAll(`.${this.classColumn}`)
            .data(d => d.values)
            .join(
                enter => enter.append("rect"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("transform", "scale(0)")
                    .remove()
            );
    }

    /**
     * Construct column text group in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateColumnLabelGroups(domNode) {
        return domNode
            .selectAll(`.${this.classColumnLabelGroup}`)
            .data(d => d.values)
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct column text leader line in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateColumnLabelLeaders(domNode) {
        return domNode
            .selectAll(`.${this.classColumnLabelLeader}`)
            .data(d => [d].filter(x => this.Label.stackIsTooTall(x, this.columnHeight(x)) && this.Label.singleIsTooTall(x, this.columnHeight(x))))
            .join(
                enter => enter.append("line"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct column text in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateColumnLabels(domNode) {
        return domNode
            .selectAll(`.${this.classColumnLabel}`)
            .data(d => [d])
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate bubble label partials in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateColumnLabelPartials(domNode) {
        return domNode
            .selectAll(`.${this.classColumnLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct Column group in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateSeries(domNode) {
        return domNode
            .selectAll(`.${this.classSeries}`)
            .data(this.data)
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct series text in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateSeriesLabels(domNode) {
        return domNode
            .selectAll(`.${this.classSeriesLabel}`)
            .data(d => this.Data.useLabels ? [d.series] : [])
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Determine if datum exists in last and visually flush right.
     * @param {d} object - d3.js stack object
     * @returns A boolean; TRUE is last series.
     */
    isLastSeries(d) {
        return this.Data.seriesLabels[this.Data.seriesLabels.length - 1] == this.Data.extractSeries(d);
    }

};

export { StackedColumnChart };
export default StackedColumnChart;
