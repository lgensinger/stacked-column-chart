# Stacked Column Chart

ES6 d3.js stacked column chart visualization.

## Install

```bash
# install package
npm install @lgv/stacked-column-chart
```

## Data Format

The following values are the expected input data structure; the keys will be used as the item's id.

```json
{
    "someKey": { "a": 445, "b": 4, "c": 10 },
    "someKey2": { "a": 300, "b": 200, "c": 5 }
}
```

## Use Module

```bash
import { StackedColumnChart } from "@lgv/stacked-column-chart";

// have some data
let data = {
    "someKey": { "a": 445, "b": 4, "c": 10 },
    "someKey2": { "a": 300, "b": 200, "c": 5 }
};

// initialize
const scc = new StackedColumnChart(data);

// render visualization
scc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| column rect | `column-click` | on click |
| column rect | `column-mouseover` | on hover |
| column rect | `column-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-stacked-column-chart` | top-level svg element |
| `lgv-content` | content inside artboard inside padding |
| `lgv-column` | column chart column |
| `lgv-column-label` | column chart column label |
| `lgv-series` | column chart series |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd stacked-column-chart

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const scc = new StackedColumnChart(data);
# add scc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
